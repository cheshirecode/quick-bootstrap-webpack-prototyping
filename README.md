# Basic Webpack + ES6 + Bootstrap/jQuery starter

Similar to https://bitbucket.org/cheshirecode/quick-es6-webpack-prototyping but tweaked further to be able to prototype right away:

- Use eslint with 'google' style.
- Reverse proxy on webpack dev server.
- Bootstrap + Bootswatch for layout.
- jQuery to save DOM manipulation effort.
- ES6 with experimental Babel for new specs goodness.

### Installation

`npm i` OR `yarn`
### Dev server

```
npm run dev
```

### Build

```
npm run build
```